// The formula of triangle area is
// Area = 1/2 * base * height
const triangleHeight = 100;
const triangleBase = 20;

const triangleArea = 1/2 * triangleBase * triangleHeight;

console.log(`${triangleArea} = ${1,5} * ${triangleHeight} * ${triangleBase}`);


